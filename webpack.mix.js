let mix = require('laravel-mix');

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.styles([
    'node_modules/admin-lte/dist/css/adminLTE.css',
    'node_modules/admin-lte/dist/css/skins/_all-skins.css'
], 'public/css/adminlte.css');