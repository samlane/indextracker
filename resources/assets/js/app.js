import './bootstrap';
import router from './routes';
import master from './views/layouts/Master';

const app = new Vue({
    el: '#app',
    router: router,
    render: h => h(master)
});

require('jquery');
