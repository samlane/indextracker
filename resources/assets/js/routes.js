import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		name: 'Dashboard',
		component: require('./views/Dashboard')
	},
];

export default new VueRouter({
	routes: routes
});